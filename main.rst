**********************************
Free Software for European Science
**********************************

Summary
=======

The principles of Free Software (FS) are well aligned with the core tenets of science, i.e., the sharing of knowledge
and the demand for reproducibility of results. Nevertheless, FS is still rather the exception than the rule for the
majority of scientific domains. The aim of the following text is to provide an introduction to the state of FS within
the European scientific community. It will mainly focus on the generation of FS by scientists as part of their research
activity and put less emphasis on discussing the use of FS within scientific institutions. The text will give an
overview about current requirements and recommendations of European and national funding agencies and potential starting
points for policy proposals.

Note 1: Unless explicitly mentioned, the text uses the term *science* in a broad sense, referring to natural, formal,
social and applied sciences as well as to humanities.

Note 2: The text is primarily written for an audience familiar with the details of the Free Software concept. Other
readers might want to read `Appendix A`_ first.

Note 3: The text is potentially biased towards Life Sciences. Input from other domains is highly welcome.

.. _`Appendix A`: `Appendix A - The concept of Free Software`_

Background
==========

Modern science considers the reproducibility of results to be an indispensible requirement for their acceptance by the
community. It is therefore regarded as a legitimate interest to analyze, reproduce and build upon the research output
of others as long as there is no undue appropriation and proper credit is provided. The latter typically happens by the
citation of the original work, which constitutes the central credit mechanism. Over the last two decades,
digitalization has started to transform a large number of scientific domains, both due to the ability to generate large
bodies of digitalized data as well as the possibilty to publish and potentially share them. After numerous discussions
that lasted for at least a decade, the `FAIR`_ (findable, accessible, interoperable, reuseable) principles
[Wilkinson_2016]_ have become the prevailing paradigm for the sharing and reuse of scientific data. It however should be
noted, that FAIR does not necessary imply *Open Data*, which would demand free and non-discriminative access to data
sets. The constantly increasing size and complexity of scientific data sets has made manual analysis and management
prohibitive and therefore requires more and more elaborate code/software tools, often produced within the same
scientific project. Importantly, the use and generation of FS is by no means the standard in this setting, although the
interests of the scientists (as stated above) would be well reflected by the `4 Freedoms`_ of FS.

Academic science in Europe is primarily tax-funded with an estimated annual budget of 100 billion EUR [#]_ . Over the
last years the European Commission (EC) has started to promote the comprehensive vision of *Open Science*, which saw its
initial description in 2016 [DOI_10_2777_061652]_ and has undergone further refinements since then. Importantly,
*Open Science* is a substantially broader paradigm than mere *Open Access* and *Open Data* and describes a more
fundamental cultural change towards an open and participatory system (defined by eight priorities identified by the
Open Science Policy Platform [DOI_10_2777_958647]_). While FS is rarely mentioned explicitly in the documents, it must
be recognized that the general notion of *Open Science* can only be attained when also the code is free and open.

.. [#] Note that the target of spending 3% of the GDP on research and development (R&D) is not met by the majority of EU
   member states. Furthermore, it includes also industrial spending, which accounts for around 2/3 of the overall R&D
   expenses [Source: EuroStat, https://ec.europa.eu/eurostat/statistics-explained/index.php/R_%26_D_expenditure]. In
   addition, funding for academic research from the EU is currently mainly implemented via the Horizon 2020 program
   (around 10 GEUR p.a.; doi:10.2761/9592), however as it is spent in the member states it is counted to their budget.

.. _`FAIR`: https://www.go-fair.org/fair-principles/


General Issues
==============

FAIR vs. FS
-----------

FAIR has by now become a universally understood (although not universally practiced) concept in regard to scientific
data. However, while the FAIR principle can in also be applied to code (i.e., software), it is important to recognize a
couple of points here:

*  Code can be FAIR but not FS (e.g., FAIR does not require an FS license)
*  Code can be FS but not FAIR (especially *Findability* and *Interoperability* can be problematic)
*  In contrast to data, code invites contributors and thus will change over time. This is something that the current
   system of scholarly communication does not provide credit for, as discussed `here`_.
*  There is little knowledge in the scientific community about licensing and the impacts on further usage.
*  The concept of dependencies between licences for different digital objects already is already present in FAIR, as
   GP-I2 formulates that vocabularies used by data objects must also comply to the FAIR criteria, so that
   interchangeability is guaranteed.
*  The issue of software sustainability has been analyzed, e.g., by [Howison_2016]_.
*  Modifications to the FAIR guidelines to make them suitable for software have been proposed [Lamprecht_2019]_.
   However this paper remains unspecific when it comes to licensing.
*  Software has been recognized as independent research output in TOP: https://cos.io/top/

.. _`here`: https://www.slideshare.net/danielskatz/fair-is-not-fair-enough-particularly-for-software-citation-availability-or-quality


European Policies
=================

Current developments
--------------------

*  The `EOSC Declaration Action List`_ states that "Software sustainability should be treated on an equal footing as
   data stewardship".
*  The "Horizon Europe Regulation Proposal" of the European Commission (EC) [COM_2018_435]_ mentions under point (5)
   that within the program "Provisions should be laid down to ensure that beneficiaries provide open access to
   peer-reviewed scientific publications, research data and other research outputs in an open and non-discriminatory
   manner, free of charge and as early as possible in the dissemination process, and to enable their widest possible use
   and re-use." This notion is implemented in Article 10. While "software" as a research output is only specifically
   mentioned in Annex V of the document, the case can be made that this is implicitly included. As usual the EU is
   rather lenient in providing exceptions from Open Science guidelines if there is a possibility to commercialize.
*  The final report and action plan "Turning FAIR into reality" by the EC expert group on FAIR data [DOI_10_2777_1524]_
   mentions "code" or "software" as an important research output. It also briefly disucsses the issues of software
   sustainability and mentions the RSE movement. However, it should be noted that "code" or "software" is primarily
   mentioned in enumerations with "data" and "metadata", but more special aspects are not discussed. FS, OSS or
   software licensing are not explicitly discussed in the text.
*  On the level of DG RTD, there seems to be a growing recognition for FOSS, at least in the context of EOSC. This is
   exemplified, e.g., by the "European Open Science Cloud (EOSC) Strategic Implementation Plan" [DOI_10.2777/202370]_,
   which states on p. 27 that "In order to be EOSC compliant, software developments will be open source by default".
*  Furthermore, the `EOSC Partnership draft proposal`_ (2020-05-27) consistently mentions "publications, data and
   software" as fixed enumeration of research outputs. However, the document is otherwise not to ambitious about FS
   but mentions it in combination with sustainability aspects on p. 47.
*  `Plan S`_ does mention software as separate entity, however only in the more detailed section (III, 1.1), not in its
   10 principles.

.. _`EOSC Declaration Action List`: https://ec.europa.eu/research/openscience/pdf/eosc_declaration-action_list.pdf
.. _`Plan S`: https://www.coalition-s.org/principles-and-implementation/
.. _`EOSC Partnership draft proposal`: https://ec.europa.eu/info/files/european-open-science-cloud-eosc_en


National Policies
=================

Germany
-------

*  The German Research Foundation (DFG) is Germany's largest scientific funding organization. It does not operate own
   infrastructure, but provides project-based fund, mainly to universities.

   *  The DFG released updated Guidelines for Safeguarding Good Scientific Practice [ISBN_978-3-96827-001-2]_ in July
      2019. These guideline ("Kodex") are considered top-level and require further implementation by the research
      institutions. Nevertheless, they provide a general normative framework. Guideline 13 states that "Software
      programmed by researchers themselves is made publicly available along with the source code" and further elaborates
      that this should be done under and "appropriate license".
   *  DFG had a call in 2016 for projects increase software sustainability in the research context. The call is rather
      broad, dealing with infrastructures and best practices, FLOSS is not specifcially mentioned:
      https://www.dfg.de/foerderung/info_wissenschaft/2016/info_wissenschaft_16_71/index.html
   *  There is a follow-up call on Software Sustainablility, which also addresses open source ("quelloffen") as an
      outcome and a target in terms of sustainability. However, it does not require a FS license.
      https://www.dfg.de/foerderung/info_wissenschaft/2019/info_wissenschaft_19_44/index.html

*  The Helmholtz Association of German Research Centres (HGF) is starting to address this issue in the context of its
   Open Science working group (HGFOS):

   *  Position paper "Zugang zu und Nachnutzung von wissenschaftlicher Software" (2017) [https://os.helmholtz.de/index.php?id=2766]
      ("Access to and re-use of scientific software", *German only*)
      This document summarizes a workshop in Dresden (11/2016) that was organized by HGFOS and included people from
      HGF, MPG, DFG and other. The *Allianz* ad-hoc group was founded here. In general the document deals with
      sustainablity (Guidelines, Incentives, Publication & Appreciation, Infrastructure (Repos), Educational/Personal,
      Legal framework). It is noteworthy that both the "Incentives" and "Legal" sections **explicitly** recommend/deal
      with OSS. Otherwise the document recommends the centers to build up structures and processes that help scientist
      ensure software sustainability. Note that this is not a binding policy document, rather a starting point for the
      centers to develop their own guidelines.
   *  HGFOS Workshop "Zugang zu und Nachnutzung von wissenschaftlicher Software" (2017) [DOI_10_2312_lis_17_01]_
      Also a product of the Dresden Workshop (s.a.), this report is more extensive than the original position paper.
      Also, in regard to software developed by researchers, this report is substantially more comprehensive than
      Katerbow *et al.*. It has in general a positive sentiment towards FS, but allows for closed soure in case of
      commercialization.
   *  Diskussionspapier "Empfehlungen zur Implementierung von Leit- und Richtlinien [...]" [DOI_10_2312_os_helmholtz_008]_
      This document is also available in a slightly abridged English version "Dealing with Research Software [...]"
      [DOI_10_2312_os_helmholtz_003]_.
      A rather detailed document that should act as a basis for discussion for the the development of the model
      guidelines for research software management at HGF centers. Contains a recommendation for the OSI-compliant use of
      OSI-compliant licenses, along with a case-by-case decision for permissive vs. share-alike. Does not discuss the
      relation between FAIR and FS.
   *  A model guideline ("Musterrichtlinie") for research software management in the individual centers was released by
      HGFOS in 2020 [DOI_10_2312_os_helmholtz_007]_. The guideline is mainly focused on organisational measures to
      promote good development practices and sustainable software. While it suggest to the centers to promote the
      involvement of their employees in FOSS projects, it also cautions on the effects of "infectious" licenses.

*  Allianz der deutschen Wissenschaftsorganisationen (*Allianz*) -> Schwerpunktinitiative "Digitale Information" ->
   Ad-hoc AG "Wissenschaftliche Software"

   *  Katerbow M et al. (03/2018) Recommendations on the development, use and provision of Research Software
      [DOI_10_5281_zenodo_1172988]_. This document provides high level recommendations on Research Software. In general
      the tone is rather positive towards FS, e.g. the use of OSS is explicitly recommended. For development, the text
      is less explicit, i.e., it states that source code should be readable and might/should be licenced under an FS
      licence, however potential commercialization is named as the key exception to this rule.
   *  Ad-hoc WG now moved into "Phase 3" and is now a real WG "Digitale Werkzeuge: Software und Dienste"
   *  Deliverable: Detailed recommendation document (Planing / Tools / Publications / Infrastructure)

*  The recent RfII report on job and carreer prospects science explicitly mentions the negative impact of propriatary
   software solution on reproducibility in science [Chapter 2.3: http://www.rfii.de/?p=3883].
*  The National Research Data Infrastructure (NFDI) is Germany's main contribution to EOSC. The current call contains
   has a clear requirement for FOSS (p. 7): https://www.dfg.de/formulare/nfdi100/nfdi100_en.pdf


France
------

The French Open Science Plan mentions only briefly a committment to treat software as a critical compontent, but
otherwise refers to the Software Heritage initiative: https://libereurope.eu/blog/2018/07/05/frenchopenscienceplan/


Other stakeholders
==================

*  Research Software Engineer (RSE) organisations: Originating from the UK in ~2014, these organisations promote that
   RSEs should be recognized and paid comparable to classical researcher roles. This goes along with the demand that
   software should be recognized as a primary research product and thus requires professionalisation of its development
   and maintenance process. While the organisations are typically sympathetic to the Free Software movement, it has to
   be recognized that this is not necessary a primary focus, but rather a derivative of the interest in software
   sustainability.

   *  Jon Switters, David Osimo; Recognising the Importance of Software in Research -- Research Software Engineers
      (RSEs), a UK Example; Jan 2019
      https://ec.europa.eu/info/sites/info/files/research_and_innovation/importance_of_software_in_research.pdf

*  Software Sustainability Institutes are organisation closely entangeled with the RSE movement and address the
   question how scientific software development can be made open and provide long-term maintenance.


Potential points for Policy Proposals
=====================================

*  Software/code should become a first class research product, equal to data and publications. This should simplify
   the discussion around policies, as each class can have it's own set of requirements (no one talk about FAIR
   publications).
*  The FAIR principles were formulated with data in mind, not software. Therefore, input should be provided for
   potential amendments for code objects in general, to ensure that these are not "black boxes" but FS.
*  The notion that the FAIR criteria can be sufficently tweaked to apply to code, need to be critically reviewed. It
   might be simpler to agree that FAIR is a basic requirement for all classes of digital objects in Open Science and
   that some classes have additional requirements on top (e.g., Plan S for publications). This would allow to keep the
   FAIR criteria to their original core and have additional requirements for code on top. This should on the one hand
   be a FOSS license and on the other hand some quality criteria. The latter one is based on the problem that FAIR
   only describes the current state of an object, but is does not describe the process that generated it. Trying to
   include this process into FAIR would bloat it severely and in addition raise the problem that the documentation
   of the process is distinct for the various object classes.
*  Github is frequently used to deposit software, however there are a number of implications:

   *  The long-term perservation and assignment of DOIs is incomplete. The possibility to push releases to Zenodo (which
      then will provide a DOI) exists, however this only creates a ZIP of the current code (not the commit tree).
   *  Everything outside of the core git functionality stays on a proprietary platform (bug reports. implementation
      decisions, etc.).

   Therefore funding acencies should be encouraged to deploy own code repositories (e.g., by enhancing git repos running
   on FS) that guarantee the continued availability of this form of scholarly communication.

*  To credibly capitalize of the sustainability feature of FS, there needs to be a realistic propect that someone
   actually maintains a software. Therefore, drafting and supporting incentivation schemes for professional software
   maintenance that does allow coders to stay in science are necessary. In simple terms this can be done via authorship
   (in addition to citation). This is a topic that would likely go alongside with the RSE community.
*  A lot of policy documents consider the FS vs proprietary licensing a decision that is up to the researcher. The EU
   Open Science Policy Pilot is at least at the point that FS is the default but with wide ranging opt-outs. The
   critical question is: How many *good* examples for proprietary licensing giving rise to a viable business are there?
   How many are there for dual-licensing or FS?
*  Another point for discussion is the use of "open-source-but-proprietary" licenses (instead of
   "closed-source-and-proprietary"). While FS is of course the goal, from a scientific perspective an open source that
   cannot be used is still better than a black-box. However, it must be clear that this is pitched as an alternative to
   proprietary licensing, only. It should never be perceived as an alternative to FS licenses if they can be applied to
   the code.


Events
======

*  Open Access Tage: An annual event in the German speaking region (CH/AT/DE), mainly catering toward people involved in
   scholarly communication (librarians, publishers, etc.) https://open-access.net/AT-DE/community/open-access-tage/


Further Reading
===============

*  FORCE11 had a temporary workgroup on software citation in 2016. Their report mainly recommends the use of PIDs that
   optimally should be version specific and appropriate credit by metadata. There not recommendation on licensing.
   DOI: https://doi.org/10.7717/peerj-cs.86
*  The Netherlands eScience Center is running a research software index, which attempt to address the "F" in FAIR:
   https://www.research-software.nl/about
*  The German Federal Ministry for Education and Research has a program called "Software-Sprint"
   [https://www.bmbf.de/foerderungen/bekanntmachung-1225.html], which funds Open Source Projects by free-lance
   programmers focusing on security or civic tech. Note that institutions like universities are excluded from
   application, nevertheless this could be interesting in special cases.


Still to check
==============

*  Geosciences and FS: https://www.listserv.dfn.de/sympa/arc/wiss-software/2019-01/msg00000.html
*  OSS at DLR: There is an *internal only* Brochure that is the authorative guideline, but the following presentation
   captures a couple of points: http://elib.dlr.de/100060/1/20151018%2520Open%2520Source%2520im%2520DLR.pdf
*  From RSE19 "FAIR Software" BoF session: https://librarycarpentry.org/Top-10-FAIR/2018/12/01/research-software/


Terms
=====

*  "FAIR": Acronym for "Findable, Accessible, Interoperable, Reuseable". Originally developed for data.

* "long tail of science": Refers to the (assumed) 80% of research done by groups that only receive 20% of the ICT
   budget: Experimental groups, small universities, humanities, etc.. As long as solutions are only made to meet the
   needs of large consortia and "light-house" projects, this research will continue to be done in spreadsheet
   applications.


References
==========

.. [Howison_2016] James Howison and Julia Bullard. 2016. Software in the scientific literature: Problems with seeing,
   finding, and using software mentioned in the biology literature. Journal of the Association for Information Science
   and Technology 67:2137–55 (2016). https://doi.org/10.1002/asi.23538
.. [Lamprecht_2019] Anna-Lena Lamprecht, Leylab Garcia, Mateuszc Kuzak, et al. Towards FAIR principles for research
   software. Data Science *in press*. https://doi.org/10.3233/DS-190026
.. [Wilkinson_2016] Mark D. Wilkinson, Michel Dumontier, IJsbrand Jan Aalbersberg, et al. The FAIR Guiding Principles
   for scientific data management and stewardship. Sci Data 5:160018. https://doi.org/10.1038/sdata.2016.18.

.. [COM_2018_435] https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=COM:2018:435:FIN
.. [DOI_10_2777_1524] https://doi.org/10.2777/1524
.. [DOI_10_2777_061652] https://doi.org/10.2777/061652
.. [DOI_10_2777_202370] https://doi.org/10.2777/202370
.. [DOI_10_2777_958647] https://doi.org/10.2777/958647
.. [DOI_10_5281_zenodo_1172988] https://doi.org/10.5281/zenodo.1172988
.. [DOI_10_2312_lis_17_01] https://doi.org/10.2312/lis.17.01
.. [DOI_10_2312_os_helmholtz_003] https://doi.org/10.2312/os.helmholtz.003
.. [DOI_10_2312_os_helmholtz_007] https://doi.org/10.2312/os.helmholtz.007
.. [DOI_10_2312_os_helmholtz_008] https://doi.org/10.2312/os.helmholtz.008

.. [ISBN_978-3-96827-001-2] https://www.dfg.de/download/pdf/foerderung/rechtliche_rahmenbedingungen/gute_wissenschaftliche_praxis/kodex_gwp_en.pdf


Appendix
========

Appendix A - The concept of Free Software
-----------------------------------------

The term "Free Software" was coined by Richard Stallman in 1986 to describe software that provides the users with the
"`4 Freedoms`_", meaning the freedom to:

*  *Use:* Run the program for any purpose
*  *Study:* Understand how a program works
*  *Share:* Redistribute copies of a program
*  *Enhance:* Change a program to suit your needs and share the modified version with others

With minor revisions the original definition is still used by both the US-based Free Software Foundation (FSF) and the
Free Software Foundation Europe (FSFE). Note that while not explicitly mentioned, the user requires access to the
source code for the second and fourth point to be fulfilled. Further note that it is implicitly assumed that the
4 Freedoms are granted via a license.

The related term "Open Source Software" was coined by the Open Source Initiative (OSI) starting 1998 to avoid the dual
meaning of "free" as in "at no cost", which was considered problematic from an economic view point. The OSI's `Open
Source Definition`_ contains ten criteria that have a stronger focus on the license of the software than on the
user.

Nevertheless, in practice both terms describe - by and large - the same set of licenses. Importantly, all frequently
used licences are approved both by the FSF and the OSI. Therefore this text gives preference to the older termer, which
- in addition - is easier to map to the scientific process.

.. _`4 Freedoms`: https://www.gnu.org/philosophy/free-sw.html
.. _`Open Source Definition`: https://opensource.org/osd


Appendix B - Use of FS in Scientific Institutions
-------------------------------------------------

As mentioned in the introduction, this very important issue is not the main focus of this document, therefore the
following list mainly acts as collection point for future use.

*  Microsoft is apparrently becoming more and more stringent in assigning the status of an "academic institution",
   which is associated with substantial discounts for licenses. To be considered as "academic institution" by Microsoft,
   an entity must be a degree-granting organisation, an entitlement that is typically only held by universities, not by
   other publicly funded or non-profit research institutions. While CERN (s/a) is a rather prominent example, similar
   issues have also been reported by other institutions. => Perform a broad assessment of licences changes across the
   larger national scientific organisatons to document the extend. Develop recommendation for migration of IT ecosystems
   that considers FS as an integral part of the solution.
*  CERN announced in June 2019 that it will look actively for solution to migrate from proprietary software to FS. The
   respective project is termed `MAlt`_ ("Microsoft Alternatives") and was triggered by the revocation of CERN's status
   as "academic institution", leading to substantially higher licensing costs.
*  Germany: In terms of use, there is no indication that FS is practicular relevant. This is complicated by the
   fragmentation within the 16 Länder (states). The "Expertenkommision Forschung und Innovation" (Expert committee
   research and innovation) [established by the federal government in 2016], reports extensively on the challenges of
   digitalization faced by universities, but does not mention FS once. On the contrary, it recommends a coordinated
   approach to procure (commercial) software licenses. EFI Gutachten 2019, pp. 101 & 104
   https://www.e-fi.de/fileadmin/Gutachten_2019/EFI_Gutachten_2019.pdf

.. _`MAlt`: https://home.cern/news/news/computing/migrating-open-source-technologies
