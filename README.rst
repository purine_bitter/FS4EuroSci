Free Software for European Science
==================================

Document summarizing the situation and current and (potential) future policies regarding Free Software within European
science.


Building
--------

The content of this repo is supposed to be build using the Python Sphinx documentation generator. However, ``.rst``
files are perfectly human-readable without it.
